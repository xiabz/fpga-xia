module LV(
	input  s_clk,
	input  s_reset,
	input  LV_detect_in,
	input  LV_ti_in,
	output Q_LV,
	
		//debug仿真用
		input  debug_reg_LV_switch,
		input  debug_reg_GLOBAL_stop,
		input  debug_reg_GLOBAL_reset,
		output debug_Q_LV_on,
		output debug_reg_LV_ti_error,
		output debug_reg_LV_ti_warning,
		output debug_reg_LV_point_warning,
		output debug_reg_LV_ready
			);
	
	reg reg_GLOBAL_stop;        //全局急停
	reg reg_GLOBAL_reset;       //全局软复位
	reg reg_LV_switch;          //LV开关
	reg Q_LV_on;                //LV输出（未延迟） 受reg_LV_point_warning和
	reg Q_LV_on_delayed;        //LV实际输出 同Q_LV
	reg reg_LV_ready;           //开机时，有delayed输出后，若有LV_detect_in，则置1
	reg reg_LV_point_warning;   //关机时，若有LV_detect_in，则置1
	reg reg_LV_ti_warning;      //任意时候，无LV_detect_in时,有LV_ti_in，则置1
	reg reg_LV_ti_error;        //开机时，有detect_in后一段时间，ti_in仍为0 则置1 软复位消去
	reg [63:0] counter_LV_1;    //Q_LV_on_delayed从Q_LV_on延时的计数器
	reg [63:0] counter_LV_2;    //ti_error的计数器 软复位置0
	parameter	CNT_LV_ON_DELAY_TIME=15; //20ns*n Q_LV_on_delayed延时时间设置
	parameter	CNT_LV_TI_DEBUG_TIME=15; //20ns*n reg_LV_ti_error时间设置


		//debug仿真用
		assign debug_Q_LV_on=Q_LV_on;
		assign debug_reg_LV_ti_error=reg_LV_ti_error;
		assign debug_reg_LV_ti_warning=reg_LV_ti_warning;
		assign debug_reg_LV_point_warning=reg_LV_point_warning;
		assign debug_reg_LV_ready=reg_LV_ready;
		always @(posedge s_clk)
		begin
			if(s_reset) 
				reg_LV_switch<=0;
			else
				begin
				reg_LV_switch  <=debug_reg_LV_switch;
				reg_GLOBAL_stop<=debug_reg_GLOBAL_stop;
				reg_GLOBAL_reset<=debug_reg_GLOBAL_reset;
				end
		end
	
	
	assign Q_LV=Q_LV_on_delayed;
	always @(posedge s_clk)
	begin
		if(s_reset)
			begin
			reg_LV_ready        <=0;
			reg_LV_point_warning<=0;
			reg_LV_ti_warning   <=0;
			reg_LV_ti_error     <=0;
			counter_LV_1        <=0;
			counter_LV_2        <=0;
			Q_LV_on             <=0;
			Q_LV_on_delayed     <=0;
			end
		else
			begin
			reg_LV_ti_error     <= reg_LV_switch && !reg_GLOBAL_reset && (reg_LV_ti_error ||(!LV_ti_in && (counter_LV_2==CNT_LV_TI_DEBUG_TIME)));
			reg_LV_ti_warning   <= !LV_detect_in && LV_ti_in;
			reg_LV_point_warning<= reg_LV_point_warning ||(LV_detect_in && !reg_LV_switch);
			reg_LV_ready        <= reg_LV_switch && (reg_LV_ready || LV_detect_in) && Q_LV_on_delayed;
			Q_LV_on             <= reg_LV_switch && !reg_GLOBAL_stop && !reg_LV_point_warning;
			Q_LV_on_delayed     <= Q_LV_on && (Q_LV_on_delayed ||(counter_LV_1==CNT_LV_ON_DELAY_TIME));
			counter_LV_1        <= (Q_LV_on)     ? (counter_LV_1 + !Q_LV_on_delayed):0 ;
			counter_LV_2        <= (LV_detect_in && !reg_GLOBAL_reset)? (counter_LV_2 + reg_LV_switch):0;
			end
	end
endmodule
