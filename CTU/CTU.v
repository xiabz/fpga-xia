module CTU(
	input  s_reset,
	input  s_clk,
	input  CTU_heating_ready_in,
	output CTU_on_out,
		
	
		//debug仿真用
		input  debug_reg_CTU_switch,
		output debug_reg_CTU_heating_start,
		output debug_reg_CTU_heating_end,
		output debug_reg_CTU_error,
		output debug_reg_CTU_p_error,
		output debug_reg_CTU_heating_timeout);
		
	reg reg_CTU_switch;          //CTU开关
	reg reg_CTU_heating_start;   //CTU加热开始标志 在CTU_on_out为1时置1，保持
	reg reg_CTU_heating_end;     //CTU加热结束标志 在CTU_on_out为1且CTU_heating_ready_in为1时置1，保持
	reg reg_CTU_heating_timeout; //CTU加热超时标志 根据start和end确定
	reg reg_CTU_error;           //正常工作时候CTU_heating_ready_in掉电故障。
	reg reg_CTU_p_error;         //【逻辑错误？】开机时，CTU_heating_ready_in有输入，CTU_on_out没输出
	reg [31:0] counter_CTU_1;    //heating_timeout计时器 
	parameter CNT_HEATING_TIME=15;//n*20ns  timeout触发时间设定
	
		//debug仿真用
		assign debug_reg_CTU_heating_start=reg_CTU_heating_start;
		assign debug_reg_CTU_heating_end=reg_CTU_heating_end;
		assign debug_reg_CTU_heating_timeout=reg_CTU_heating_timeout;
		assign debug_reg_CTU_error=reg_CTU_error;
		assign debug_reg_CTU_p_error=reg_CTU_p_error;
		always @(posedge s_clk)
		begin
			if(s_reset)  reg_CTU_switch<=0;
			else         reg_CTU_switch<=debug_reg_CTU_switch;
		end


	assign CTU_on_out = reg_CTU_switch && (!reg_CTU_error) && (!reg_CTU_p_error);
	always @(posedge s_clk)
	begin
		if(s_reset)
			begin 
			reg_CTU_error           <=1'b0;
			reg_CTU_p_error         <=1'b0;
			reg_CTU_heating_end	   <=1'b0;
			reg_CTU_heating_start   <=1'b0;
			reg_CTU_heating_timeout <=1'b0;
			counter_CTU_1           <=0;
			end
		else
			begin
				reg_CTU_error          <= reg_CTU_switch && (reg_CTU_error || (!CTU_heating_ready_in && reg_CTU_heating_end));
//				reg_CTU_p_error        <= reg_CTU_switch && (reg_CTU_p_error || (CTU_heating_ready_in && !CTU_on_out)); //【待修改】   
				reg_CTU_heating_end    <= reg_CTU_switch && (reg_CTU_heating_end || (CTU_heating_ready_in && CTU_on_out));
				reg_CTU_heating_start  <= reg_CTU_switch && (reg_CTU_heating_start || CTU_on_out);
				reg_CTU_heating_timeout<= reg_CTU_switch && (reg_CTU_heating_timeout || (counter_CTU_1==CNT_HEATING_TIME));				
				//加热时间计时部分 在switch为1时开始计时 bug：start和end波形不正常多次变化会怎样？
				if(reg_CTU_switch)
					counter_CTU_1 <= counter_CTU_1+(reg_CTU_heating_start && !reg_CTU_heating_end);
				else
					counter_CTU_1 <= 0;
			end
	end
endmodule
