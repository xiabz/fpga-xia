//Legal Notice: (C)2013 Altera Corporation. All rights reserved.  Your
//use of Altera Corporation's design tools, logic functions and other
//software and tools, and its AMPP partner logic functions, and any
//output files any of the foregoing (including device programming or
//simulation files), and any associated documentation or information are
//expressly subject to the terms and conditions of the Altera Program
//License Subscription Agreement or other applicable license agreement,
//including, without limitation, that your use is for the sole purpose
//of programming logic devices manufactured by Altera and sold by Altera
//or its authorized distributors.  Please refer to the applicable
//agreement for further details.

module INPUT_DBC (
	s_clk,
	s_reset,
	data_in,
	data_out
);

parameter WIDTH = 32;           // set to be the width of the bus being debounced
parameter TIMEOUT = 50000;      // number of input clock cycles the input signal needs to be in the active state
parameter TIMEOUT_WIDTH = 16;   // set to be ceil(log2(TIMEOUT))

input wire s_clk;
input wire s_reset;

input wire [WIDTH-1:0] data_in;
output wire [WIDTH-1:0] data_out;

reg [TIMEOUT_WIDTH-1:0] counter [0:WIDTH-1];
reg [WIDTH-1:0] data;
wire counter_rst [0:WIDTH-1]; // counter reset
wire counter_en [0:WIDTH-1];  // counter enable

// need one counter per input to debounce
genvar i;
generate for (i = 0; i < WIDTH; i = i+1)
begin:  debounce_counter_loop

	always @ (posedge s_clk)
	begin
		if (s_reset)
		begin
			counter[i] <= 0;
			data[i] <= 0;
		end
		else
		begin
			if (counter_rst[i] == 1)  // resetting the counter needs to win
			begin
				counter[i] <= 0;
			end
			else if (counter_en[i] == 1)
			begin
				counter[i] <= counter[i] + 1'b1;
			end

			if(data[i]) 
				data[i] <= (counter[i] >= TIMEOUT) ? 0 : 1;
			else
				data[i] <= (counter[i] >= TIMEOUT) ? 1 : 0;
				if(counter[i]>=TIMEOUT) 
					counter[i] = 0;
		end
	end

	assign counter_rst[i] = (data[i] == 0) ? (data_in[i] == 0 ? 1 : 0) : (data_in[i] == 1 ? 1 : 0);
	assign counter_en[i]  = (data[i] == 0) ? (data_in[i] == 1 && counter[i] < TIMEOUT) : (data_in[i] == 0 && counter[i] < TIMEOUT);
	assign data_out[i]    = data[i];

end
endgenerate

endmodule