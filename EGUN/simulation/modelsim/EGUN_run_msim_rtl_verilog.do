transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/Users/xiabz/Desktop/xia/EGUN {C:/Users/xiabz/Desktop/xia/EGUN/EGUN.v}

vlog -vlog01compat -work work +incdir+C:/Users/xiabz/Desktop/xia/EGUN/simulation/modelsim {C:/Users/xiabz/Desktop/xia/EGUN/simulation/modelsim/EGUN.vt}

vsim -t 1ps -L altera_ver -L lpm_ver -L sgate_ver -L altera_mf_ver -L altera_lnsim_ver -L cyclonev_ver -L cyclonev_hssi_ver -L cyclonev_pcie_hip_ver -L rtl_work -L work -voptargs="+acc"  EGUN_vlg_tst

add wave *
view structure
view signals
run -all
