module EGUN(
		input s_clk,
		input s_reset,
		input EGUN_fila_in,				//高电平故障
		input EGUN_rfc_vac_in,			//高电平故障
		output Q_EGUN_pwm,
		output Q_EGUN_on,
		
			//debug仿真用
			input [15:0]debug_reg_EGUN_half_volt_upper,
			input [15:0]debug_reg_EGUN_full_volt_upper,
			input debug_reg_EGUN_switch,
			input debug_reg_EGUN_h2f_switch,
			input debug_reg_EGUN_active_mode,
			input [63:0]debug_reg_EGUN_heating_time,
			output debug_reg_EGUN_debug_fila,	
			output debug_reg_EGUN_debug_vac,		
			output debug_reg_EGUN_applying_volts,
			output debug_reg_EGUN_half_volt_reach,
			output debug_reg_EGUN_full_volt_reach,
			output debug_reg_EGUN_ready		
			);
		
	
	
	
		reg [15:0]reg_EGUN_full_volt_upper;   //全压上位机设定值
		reg [15:0]reg_EGUN_half_volt_upper;   //半压上位机设定值
		reg reg_EGUN_switch;                  //EGUN开关    1开0关
		reg reg_EGUN_h2f_switch;              //全压半压开关 1全压 0半压
		reg reg_EGUN_active_mode;             //激活模式 1时FPGA控制电压 0时额外硬件控制（电压寄存器置零）
		reg [63:0]reg_EGUN_heating_time;      //全压后预热时间
		reg [15:0]reg_EGUN_volts_accurate;    //额外检测电路获取的实时电压
		reg reg_EGUN_power_on;                //同Q_EGUN_on
		reg [15:0]reg_EGUN_pwm_volt_applied;  //实际应用的电压值 0~10000线性映射到0~20v 
		reg [15:0]reg_EGUN_pwm_volt;			  //应该应用的电压值 0~10000线性映射到0~20v
		reg [15:0]reg_EGUN_full_volt_sp;      //全压目标值  (0~10000)	默认5000
		reg [15:0]reg_EGUN_half_volt_sp;      //半压目标值	(0~10000)	默认2500
		reg reg_EGUN_applying_volts;        //加压状态指示 0时还在加压 1时加压完成(包括半压全压)
		reg reg_EGUN_half_volt_reach;       //半压已到指示  若之后更改半压预设值使之未到，仍保持1
		reg reg_EGUN_full_volt_reach;	      //全压已到指示 
		reg reg_EGUN_ready;                 //准备完成指示 
		reg reg_EGUN_debug_fila;				//fila输入指示器
		reg reg_EGUN_debug_vac;					//vac输入指示器	
		reg [15:0] counter1;                //pwm波中占空比的counter  0映射到0v 2500映射到5v 5000映射到10v
		parameter CNT_EGUN_PWM_CYCLE=10000; //pwm占空比clk总数,这里50m/5K=10000	
		reg [15:0] counter2;                        //pwm电压设定值变化时间的计数器。
		parameter CNT_EGUN_PWM_ADJ_STEP_TIME=10000;//10000=0.2ms 500000=10ms 每10msPWM电压更新一次	
		reg [63:0] counter3;								//ready信号计时 也可用作加热倒计时
		

			//debug仿真用			
			assign debug_reg_EGUN_debug_fila     =reg_EGUN_debug_fila;	
			assign debug_reg_EGUN_debug_vac      =reg_EGUN_debug_vac;		
			assign debug_reg_EGUN_applying_volts =reg_EGUN_applying_volts;
			assign debug_reg_EGUN_half_volt_reach=reg_EGUN_half_volt_reach;
			assign debug_reg_EGUN_full_volt_reach=reg_EGUN_full_volt_reach;
			assign debug_reg_EGUN_ready          =reg_EGUN_ready;
			always @(posedge s_clk)
			begin
			if(s_reset)
				begin
				reg_EGUN_switch<=0;         
				reg_EGUN_h2f_switch<=0;     
				reg_EGUN_active_mode<=0;    
				reg_EGUN_heating_time<=0;   
				reg_EGUN_half_volt_upper<=0;
				reg_EGUN_full_volt_upper<=0;
				end
			else
				begin
				reg_EGUN_switch         <=debug_reg_EGUN_switch;
				reg_EGUN_h2f_switch     <=debug_reg_EGUN_h2f_switch;
				reg_EGUN_active_mode    <=debug_reg_EGUN_active_mode;
				reg_EGUN_heating_time   <=debug_reg_EGUN_heating_time;
				reg_EGUN_half_volt_upper<=debug_reg_EGUN_half_volt_upper;
				reg_EGUN_full_volt_upper<=debug_reg_EGUN_full_volt_upper;
				end
			end


		
//main
assign Q_EGUN_on = reg_EGUN_power_on;		
always @(posedge s_clk)
	begin
		if(s_reset)
			begin
			reg_EGUN_full_volt_sp    <=5000;
			reg_EGUN_half_volt_sp    <=2500;
			reg_EGUN_pwm_volt        <=10;
			reg_EGUN_applying_volts  <=0;
			reg_EGUN_half_volt_reach <=0;
			reg_EGUN_full_volt_reach <=0;
			end
		else
			begin
			reg_EGUN_power_on    <= (!EGUN_fila_in) && (!EGUN_rfc_vac_in) && reg_EGUN_switch;
			reg_EGUN_debug_fila  <= EGUN_fila_in;
			reg_EGUN_debug_vac   <= EGUN_rfc_vac_in;
			reg_EGUN_pwm_volt    <= (reg_EGUN_h2f_switch)? reg_EGUN_full_volt_sp : reg_EGUN_half_volt_sp;
			reg_EGUN_full_volt_sp<= (reg_EGUN_full_volt_upper>4500 && reg_EGUN_full_volt_upper <5500)? reg_EGUN_full_volt_upper:reg_EGUN_full_volt_sp;
			reg_EGUN_half_volt_sp<= (reg_EGUN_half_volt_upper>2000 && reg_EGUN_half_volt_upper <3000)? reg_EGUN_half_volt_upper:reg_EGUN_half_volt_sp;
			reg_EGUN_applying_volts <=reg_EGUN_switch && (reg_EGUN_pwm_volt_applied==reg_EGUN_pwm_volt)? 1'b1:1'b0;
			reg_EGUN_half_volt_reach<=reg_EGUN_switch && (!reg_EGUN_h2f_switch && reg_EGUN_applying_volts)?1'b1:reg_EGUN_half_volt_reach;
			reg_EGUN_full_volt_reach<=reg_EGUN_switch && (reg_EGUN_h2f_switch  && reg_EGUN_applying_volts)?1'b1:reg_EGUN_full_volt_reach;
			
			end
	end
	


	
//PWM波生成 	
assign Q_EGUN_pwm=reg_EGUN_switch && reg_EGUN_active_mode && ((counter1<reg_EGUN_pwm_volt_applied)?1'b1:1'b0);	
always @(posedge s_clk)
begin    
	if(s_reset) 
		counter1<=0;
	else
	begin 	
		if(counter1>=CNT_EGUN_PWM_CYCLE)
			counter1 <= 0;	
		else
			counter1 <= counter1+16'b1;	
	end 
end 

//PWM占空比值(电压)逼近
always @(posedge s_clk)
begin
	if(s_reset)
		begin
		counter2 <= 0;
		reg_EGUN_pwm_volt_applied<=1;
		end
	else
	begin
		if(counter2>=CNT_EGUN_PWM_ADJ_STEP_TIME)
			begin
			counter2<=0;
			if(reg_EGUN_pwm_volt_applied!=reg_EGUN_pwm_volt)
				reg_EGUN_pwm_volt_applied <= (reg_EGUN_pwm_volt_applied<reg_EGUN_pwm_volt)? reg_EGUN_pwm_volt_applied+16'b1 : reg_EGUN_pwm_volt_applied-16'b1;
			else
				reg_EGUN_pwm_volt_applied <= reg_EGUN_pwm_volt_applied;
			end
		else 
			counter2<=counter2+16'b1;
	end
end
	
//ready信号
always @(posedge s_clk)
begin
	if(s_reset)
		begin
		reg_EGUN_ready <= 0;
		counter3<=0;
		end
	else 
	begin
		if(!reg_EGUN_full_volt_reach)
		counter3<=0;
		else
		begin
			if(counter3>=reg_EGUN_heating_time)
				reg_EGUN_ready<=1;
			else
				counter3<=(reg_EGUN_ready)?1'b0:counter3+64'b1;	
		end
	end
end
	
	
	
endmodule
		
		
